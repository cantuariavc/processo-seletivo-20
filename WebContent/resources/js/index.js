new Vue({
	el: "#app",
	data: {
		listaFuncionarios: [],
		funcionario: {
			id: 0,
			nome: '',
			email: '',
			salario: 0,
			idade: 0,
			setor: {
				id: 0,
				nome: ''
			}
		},
		listaSetores: []
	},
	
	created: function() {
		let vm = this;
		vm.buscaFuncionarios();
		vm.buscaSetores();
	},
	
	methods: {
		
		//FUNCIONARIO
		
		abreModalCadastroFuncionario() {
			const vm = this;
			
			$("#tituloModalFuncionario").text("Cadastro de funcionário");	
			$("#botaoModalFuncionario").text("Cadastrar Funcionário");
			
			vm.funcionario = {
				id: 0,
				nome: '',
				email: '',
				salario: 0,
				idade: 0,
				setor: {
					id: 0,
					nome: ''
				}
			};
			
		},
		
		abreModalEdicaoFuncionario(funcionario) {
			const vm = this;
						
			$("#tituloModalFuncionario").text("Edição de funcionário");
			$("#botaoModalFuncionario").text("Atualizar Funcionário");
			vm.recuperaFuncionario(funcionario);
		},
		
		onSubmitFuncionario() {
			const vm = this;
			
			switch ($("#botaoModalFuncionario").text()) {
				case "Cadastrar Funcionário":
					vm.cadastraFuncionario();
					break;
				
				case "Atualizar Funcionário":
					vm.atualizaFuncionario();
					break;
			}
		},
		
		buscaFuncionarios() {
			const vm = this;
			axios.get("/funcionarios/rs/funcionarios")
				.then(response => vm.listaFuncionarios = response.data)
				.catch(() => vm.mostraAlertaErro("Erro interno", "Não foi possível listar natureza de serviços"));
		},
		
		cadastraFuncionario() {
			const vm = this;
			
			let funcionario = vm.funcionario;
			delete funcionario["id"];
			
			console.log(funcionario);
			
			axios.post("/funcionarios/rs/funcionarios", JSON.stringify(funcionario), { headers: { 'Content-Type': 'application/json;charset=UTF-8' }})
				 .then(() => {
					vm.listaFuncionarios.push(vm.funcionario);
					vm.buscaFuncionarios();
				 })
				 .catch(() => vm.mostraAlertaErro("Erro interno", "Não foi possível listar natureza de serviços"));
			
			$('#modalFuncionario').modal('hide');
		},
		
		recuperaFuncionario(funcionario) {
			const vm = this;
			vm.funcionario = funcionario;
			
			$('#modalFuncionario').modal('show');
		},
		
		atualizaFuncionario() {
			const vm = this;
			
			axios.put(`/funcionarios/rs/funcionarios/${vm.funcionario.id}`, JSON.stringify(vm.funcionario), { headers: { 'Content-Type': 'application/json;charset=UTF-8' }})
				 .catch(() => vm.mostraAlertaErro("Erro interno", "Não foi possível listar natureza de serviços"));
			
			$('#modalFuncionario').modal('hide');
		},
		
		deletaFuncionario(funcionarioId, index) {
			const vm = this;
			
			axios.delete(`/funcionarios/rs/funcionarios/${funcionarioId}`)
				 .then(vm.listaFuncionarios.splice(index,1))
				 .catch(() => vm.mostraAlertaErro("Erro interno", "Não foi possível listar natureza de serviços"));
			
			$('#modalFuncionario').modal('hide');
		},
		
		
		// SETOR
		
		abreModalCadastroSetor() {
			const vm = this;
			
			$("#tituloModalSetor").text("Cadastro de Setor");	
			$("#botaoModalSetor").text("Cadastrar Setor");
			
			vm.funcionario.setor = {
				id: 0,
				nome: ''
			};
		},
		
		onSubmitSetor() {
			const vm = this;
			
			switch ($("#botaoModalSetor").text()) {
				case "Cadastrar Setor":
					vm.cadastraSetor();
					break;
				
				case "Atualizar Setor":
					vm.atualizaSetor();
					break;
			}
		},
		
		selecionaSetor() {
			const vm = this;
			
			console.log(vm.funcionario.setor);
		},
		
		buscaSetores() {
			const vm = this;
			axios.get("/funcionarios/rs/setores")
				.then(response => vm.listaSetores = response.data)
				.catch(() => vm.mostraAlertaErro("Erro interno", "Não foi possível listar natureza de serviços"));
		},
		
		cadastraSetor() {
			const vm = this;
			
			let setor = vm.funcionario.setor;
			delete setor["id"];
			
			axios.post("/funcionarios/rs/setores", JSON.stringify(setor), { headers: { 'Content-Type': 'application/json;charset=UTF-8' }})
				 .then(() => {
					vm.listaSetores.push(setor);
					vm.buscaSetores();
				 })
				 .catch(() => vm.mostraAlertaErro("Erro interno", "Não foi possível listar natureza de serviços"));
			
			$('#modalSetor').modal('hide');
		},
	}
});
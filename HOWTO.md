# Processo-seletivo-20

##### Ferramentas Utilizadas
* mysql v8.0.23
* Eclipse IDE for Enterprise Java and Web Developers v4.19.0
* Apache Tomcat v9.0
	
##### Pré-requisitos
* Ter o mysql instalado, com o usuário 'root' e a senha 'root'. Se não tiver esse usuário, atualize as credenciais em src/META-INF/persistence.xml
* Ter a base de dados 'funcionarios_prova' no banco de dados mysql
* Ter o maven atualizado com o projeto
* Ter o servido Tomcat rodando pelo Eclipse IDE
	
##### Acessar a Aplicação
Para acessar a aplicação, acesse a url 'localhost:8080/funcionarios/' com o servidor rodando.